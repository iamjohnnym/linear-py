import pytest  # type: ignore

from linear_search import __version__, position, sort_list, get_index_of_target


def test_version():
    assert __version__ == '0.1.0'


def test_sort_list_asc():
    unsorted_list = [5,1,7,2,8,4,6,3,9,0]
    order = "asc"
    sorted_list = sort_list(unsorted_list, order)
    assert sorted_list == [0,1,2,3,4,5,6,7,8,9]


def test_sort_list_desc():
    unsorted_list = [5,1,7,2,8,4,6,3,9,0]
    order = "desc"
    sorted_list = sort_list(unsorted_list, order)
    assert sorted_list == [9,8,7,6,5,4,3,2,1,0]


def test_sort_list_asc_default_arg():
    unsorted_list = [5,1,7,2,8,4,6,3,9,0]
    sorted_list = sort_list(unsorted_list)
    assert sorted_list == [0,1,2,3,4,5,6,7,8,9]


def test_sort_list_raised_value_error():
    unsorted_list = [5,1,7,2,8,4,6,3,9,0]
    with pytest.raises(ValueError) as err:
        sort_list(unsorted_list, 'fail')


def test_get_index_of_target():
    target_index = get_index_of_target([5,1,7,2,8,4,6,3,9,0], 7)
    assert target_index == 2


def test_get_index_of_target_try_except_loop():
    target_index = get_index_of_target([5,1,7,2,8,4,6,3,9,0], 22)
    assert target_index == 8


def test_get_index_of_target_lowest_target():
    target_index = get_index_of_target([5,1,7,2,8,4,6,3,9,0], -4)
    assert target_index == -1


def test_position_value_error_exception():
    arr = [1,2,3,4,5,6,7,8,9,0]
    target = 5
    with pytest.raises(ValueError) as err:
        position(arr, "bloop", target)
    assert "Sorting order must be 'asc', or 'desc'" == str(err.value)


def test_position_one_asc():
    arr = [1,2,3,4,5,6,7,8,9,0]
    target = 5
    target_index = position(arr, "asc", target)
    assert 5 == target_index


def test_position_one_desc():
    arr = [1,2,3,4,5,6,7,8,9,0]
    target = 5
    target_index = position(arr, "desc", target)
    assert 4 == target_index


def test_position_two_asc():
    arr = [15,122,5,85,-22,-75,16,-1]
    target = -22
    target_index = position(arr, "asc", target)
    assert 1 == target_index


def test_position_two_desc():
    arr = [15,122,5,85,-22,-75,16,-1]
    target = -22
    target_index = position(arr, "desc", target)
    assert 6 == target_index


def test_position_three_asc():
    arr = [-1,18,57,-18,34,-188,-1000,1000,99]
    target = -1
    target_index = position(arr, "asc", target)
    assert 3 == target_index


def test_position_three_desc():
    arr = [-1,18,57,-18,34,-188,-1000,1000,99]
    target = -1
    target_index = position(arr, "desc", target)
    assert 5 == target_index
