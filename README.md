# Linear Search

Returns the index value for a desired value from a sorted list from a given
unsorted list.

## Set Up Project for Use

Follow these instructions to run from Docker or from source.

### Docker

The following two commands will build the image and then run the image.  The
output of the run will be `pylint` and then `pytest`.

```bash
docker build . -t linear-search
docker run -ti linear-search
```

### Source

Requires the `poetry` package manager to install and use.

- [poetry](https://python-poetry.org/)

```bash
pip install --user poetry
```

Once poetry is installed, run the following commands to setup and install the
required packages within a virtual environment.

```bash
poetry install
```

Once the environment is set up.  You can simply run the program by executing
poetry.

```bash
poetry run pylint linear_search
poetry run pytest
```
